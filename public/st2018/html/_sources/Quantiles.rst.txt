Calcul des quartiles
--------------------

Soit :math:`X_N={x_1,...x_N}` un échantillon trié par ordre croissante
de taille N

.. math:: x_{n-1}\le x_{n}

Les quartiles sont les valeurs de l'échantillon qui sont placées au
quart et au trois quart de l'échantillon. Il y a plusieurs façon de les
définir selon la convention d'arrondi que l'on se fixe. Une convention
usuelle consiste à prélever les quartiles dans l'échantillon en
sélectionnant l'indice arrondi à l'entier supérieur.

.. math:: Q1=X_{\lceil \frac{N}{4} \rceil}

.. math:: Q3=X_{\lceil \frac{3N}{4} \rceil}

La fonction de répartition et son inverse
-----------------------------------------

Générons une loi de Poisson, une loi exponentielle et une loi de
Rayleigh, à l'aide du module `scipy.stats`

.. code:: 

    P = st.poisson(4)
    E = st.expon(scale=2.)
    R = st.rayleigh(scale=4)

La fonction de répartition d'une variable aléatoire :math:`X` est
définie comme la probabilité que la variable soit inférieure à une
valeur :math:`x` argument de la fonction de répartition

.. math:: F_X(x)=\mathbb{P}(X \le x)

.. code:: 

    fig=figure(figsize=(14,7))
    ax1 = fig.add_subplot(121)
    x = linspace(0,20,100)
    ax1.plot(x,P.cdf(x),lw=2,color='b',label='Loi de Poisson')
    ax1.plot(x,E.cdf(x),lw=2,color='r',label='Loi exponentielle')
    ax1.plot(x,R.cdf(x),lw=2,color='g',label='Lois de Rayleigh')
    ax1.grid()
    ax1.legend(loc='best')
    xlabel('x')
    ylabel('Proba[X<x]')
    plt.title(u'Fonction de répartition  (cdf)',fontsize=18)
    x = linspace(0,1,100)
    ax2 = fig.add_subplot(122)
    ax2.plot(x,P.ppf(x),lw=2,color='b',label='Loi de Poisson')
    ax2.plot(x,E.ppf(x),lw=2,color='r',label='Loi exponentielle')
    ax2.plot(x,R.ppf(x),lw=2,color='g',label='Loi de Rayleigh')
    plt.title(u'Quantiles (ppf)',fontsize=18)
    ylabel('x')
    xlabel('Proba[X<x]')
    ax2.grid()
    ax2.legend(loc='best')




.. parsed-literal::





.. image:: QuantilesE_files/QuantilesE_4_1.png


La fonction de répartition est positive non décroissante et admet un
inverse noté

.. math:: F_X^{-1}(x)

on a par définition

.. math:: F_X^{-1}(F_X(x))=x

La fonction inverse de la fonction de répartition permet de calculer les
quantiles de la loi.

.. code:: 

    y=E.ppf(E.cdf(x))
    print(np.allclose(x-y,0))


.. parsed-literal::

    True

