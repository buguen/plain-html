.. code:: 

    import scipy.stats as st 
    import rpy2.robjects
    %pylab inline


.. parsed-literal::

    Populating the interactive namespace from numpy and matplotlib


.. parsed-literal::

    /home/uguen/anaconda2/lib/python2.7/site-packages/IPython/core/magics/pylab.py:161: UserWarning: pylab import has clobbered these variables: ['draw_if_interactive', 'random', 'bytes', 'power']
    `%matplotlib` prevents importing * from pylab and numpy
      "\n`%matplotlib` prevents importing * from pylab and numpy"


Boites à Moustaches
===================

On a vu que la médiane et les quartiles sont mieux à même de représenter
un jeu de données spécialement si ce jeu de données **s'écarte de la loi
normale**.

Il est d'usage de représenter **synthétiquement** les données à l'aide
d'une **boîte à moustaches** , ou boîte de
`Tukey <https//fr.wikipedia.org/wiki/John_Tukey>`__

-  Une boîte à moustaches est centrée sur la médiane.
-  Les frontières de la boîte sont les premier et troisième quartiles
   :math:`Q_1` et :math:`Q_3`
-  L'extrémité de la moustache inférieure est la valeur **minimum dans
   les données** qui est supérieure à la frontière basse :

   .. math:: Q1 - 1.5*(Q3-Q1) 

-  L'extrémité de la moustache supérieure est la valeur **aximale dans
   les données** qui est supérieure à la frontière haute :

   .. math:: Q3 + 1.5*(Q3-Q1) 

-  A noter que les moustaches ne peuvent avec ces définitions ni être
   inférieure au minimum de l'échantillon, ni supérieure au maximum de
   l'échantillon
-  En revanche, il put exister des données à l'extérieur des moustaches

Une boite a moustache permet d'apprécier la symétrie ou la non symétrie
de l'échantillon, elle permet aussi d'identifier la présence ou
l'absence de données trés écartées de la médiane (valeurs atypique,
outliers )

Construisons un échantillon de données correspondant aux tailles d'un
ensemble d'individus. L'échantillon est construit en prenant un
échantillon uniforme dans l'intervalle [140cm,180cm] et en ajoutant 2
points atypiques de 90cm et 220cm.

.. code:: 

    import numpy as np
    import pandas as pd
    np.random.seed(1234)
    import scipy.stats as st
    taille=np.hstack((np.random.randint(140,180,10),np.array([90,220])))
    taille = np.sort(taille)
    print taille


.. parsed-literal::

    [ 90 152 155 159 163 164 166 166 170 170 178 220]


On récupère les quantiles **[min Q1 mediane Q3 max]** à l'aide de la
fonction `quantile` de `R`

On convertit le tableau taille est une `Series` pandas, ce qui a pour
effet d'ajouter un index à chacune des valeurs.

.. code:: 

    staille =pd.Series(taille)
    staille




.. parsed-literal::

    0      90
    1     152
    2     155
    3     159
    4     163
    5     164
    6     166
    7     166
    8     170
    9     170
    10    178
    11    220
    dtype: int64



On bénéficie ainsi de méthode de statistiques descriptives comme
`describe` équivalent du `summary` de `R`

.. code:: 

    staille.describe()




.. parsed-literal::

    count     12.000000
    mean     162.750000
    std       28.794333
    min       90.000000
    25%      158.000000
    50%      165.000000
    75%      170.000000
    max      220.000000
    dtype: float64



En passant au langage R

.. code:: 

    %Rpush taille
    q = %R quantile(taille)
    print q


.. parsed-literal::

    [  90.  158.  165.  170.  220.]


.. code:: 

    %R summary(taille)




.. parsed-literal::

    array([  90.  ,  158.  ,  165.  ,  162.75,  170.  ,  220.  ])



La largeur de la boîte est l'interquartile

.. math:: \textrm{IQ}=Q_3-Q_1

Les moustaches sont définies par les valeurs :

.. math:: a_1=Q_1-1.5\textrm{IQ}

.. math:: a_2=Q_3+1.5\textrm{IQ}

On fait **saturer les moustaches** sur les valeurs minimales et
maximales de l'échantillon incluses dans l'intervalle :math:`[a_1,a_2]`

.. code:: 

    taille.sort()
    taille




.. parsed-literal::

    array([ 90, 152, 155, 159, 163, 164, 166, 166, 170, 170, 178, 220])



.. code:: 

    t2=taille.reshape(2,len(taille)/2)
    print t2


.. parsed-literal::

    [[ 90 152 155 159 163 164]
     [166 166 170 170 178 220]]


.. code:: 

    np.median(taille)




.. parsed-literal::

    165.0



.. code:: 

    np.median(t2[0,:])




.. parsed-literal::

    157.0



.. code:: 

    np.median(t2[1,:])




.. parsed-literal::

    170.0



.. code:: 

    Q1 = q[1]
    Q3 = q[3]
    IQ = Q3-Q1
    print IQ


.. parsed-literal::

    12.0


.. code:: 

    # calcul des moustaches
    a1 = Q1-1.5*IQ
    a2 = Q3+1.5*IQ
    print a1
    print a2


.. parsed-literal::

    140.0
    188.0


Affichons maintenant la boîte à moustache telle qu'elle est produite
dans `R` à l'aide de la fonction `boxplot`.

On utilise ici la macro ̀%Rpush pour "pousser" une variable du notebook
python vers `R`

.. code:: 

    %Rpush taille

.. code:: 

    f = %R boxplot(taille)



.. image:: 5-BoitesAMoustacheE_files/5-BoitesAMoustacheE_21_0.png


.. code:: 

    staille.plot(kind='box')
    plt.ylim(90,225)





.. parsed-literal::

    (90, 225)




.. image:: 5-BoitesAMoustacheE_files/5-BoitesAMoustacheE_22_1.png


Les valeurs qui sont **au delà des moustaches** sont en général
intéressantes car elle sont très **improbables** dans l'hypothèse d'une
distribution sous-jacente **Gaussienne**.

On choisit donc de représenter explicitement chacune de ces valeurs
rares **individuellement** par un cercle

.. code:: 

    np.percentile(taille,50)




.. parsed-literal::

    165.0



.. code:: 

    np.percentile(taille,25,interpolation='higher')




.. parsed-literal::

    159



.. code:: 

    np.percentile(taille,75)




.. parsed-literal::

    170.0



.. code:: 

    taille




.. parsed-literal::

    array([ 90, 152, 155, 159, 163, 164, 166, 166, 170, 170, 178, 220])



.. code:: 

    x=np.array([1,2,3,4,5,6])

.. code:: 

    np.percentile(x,25,interpolation='lower')




.. parsed-literal::

    2



Un exemple avec la dataset IRIS (utilisé en TP )
------------------------------------------------

Dans cet exemple, on combine un affichage de `boxplot` avec
`swarmplot` qui permet d'afficher en superposition l'intégralité du
jeu de données.

.. code:: 

    import numpy as np
    import seaborn as sns
    import matplotlib.pyplot as plt
    
    sns.set(style="ticks")
    
    f, ax = plt.subplots(figsize=(7, 6))
    
    # Charge le dataset iris
    iris = sns.load_dataset("iris")
    
    # Affiche la période orbitale horzontalement 
    sns.boxplot(x="sepal_length", y="species", data=iris,
                whis=np.inf, palette="vlag")
    
    # Affiche chaque observation en surimpression
    sns.swarmplot(x="sepal_length", y="species", data=iris,
                  size=2, color=".3", linewidth=0)
    
    # Affine la  presentation
    ax.xaxis.grid(True)
    ax.set(ylabel="")
    sns.despine(trim=True, left=True)



.. image:: 5-BoitesAMoustacheE_files/5-BoitesAMoustacheE_31_0.png

