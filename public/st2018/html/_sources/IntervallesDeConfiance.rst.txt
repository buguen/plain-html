Intervalle de Confiance
=======================

En pratique on ne dispose pas d'un grand nombre d'essais comme dans la
simulation réalisée précédemment `ici <Estimation.html#>`__

On doit se résoudre à estimer la valeur de :math:`\mu` à l'aide du seul
échantillon de taille :math:`N` dont on dispose. On aimerait affecter à
cet **estimateur** une indication de la **confiance** qu'il faut lui
accorder.

-  Cette **confiance** devra dépendre de la taille de l'échantillon
-  On parlera aussi de **marge d'erreur**
-  L'idée consiste à indiquer, non pas la valeur ponctuelle de
   l'estimateur, mais un **intervalle** pour lequel on pourra affirmer
   que dans la perspective **"fréquentiste"** de la répétition
   (hypothétique) de l'expérience aléatoire un très grand nombre de
   fois, dans X% des cas, la **vraie** valeur de :math:`\mu` sera dans
   **l'intervalle de confiance**.
-  Il faut prendre son parti que l'estimation proposée soit **inexacte**
   dans (100-X)% des cas.
-  La fixation de la valeur de :math:`X` est liée au **contexte
   applicatif** et souvent au **risque** auquel est associée
   l'estimation.

Supposons que l'on dispose d'un échantillon de v.a indépendantes

.. math:: X_N=\{x_1,...,x_N\}

issuent d'une population de moyenne :math:`\mu` et d'écart type
:math:`\sigma_X`

-  On exploite la variable pivotable :math:`Z` distribuée selon la loi
   normale centrée réduite

.. math:: Z = \frac{\bar{X}-\mu}{\sigma_X/\sqrt{N}} = \sqrt{N} \frac{\bar{X}-\mu}{\sigma_X} 

L'intervalle de confiance :math:`[u_{\alpha/2},u_{1-\alpha/2}]` à
:math:`100(1-\alpha)\%` est défini comme l'intervalle (symétrique autour
de l'origine) qui assure que :math:`100(1-\alpha)\%` des valeurs de
:math:`Z` y soient incluses.

.. math:: \mathbb{P}(u_{\alpha/2} \le Z \le u_{1-\alpha/2})= 1-\alpha

En partique :

-  On prendra bien souvent :math:`100\alpha= 5\%`
-  On parlera d'intervalle de confiance à :math:`95\%`
-  On verra apparaître la valeur numérique (qu'il est utile de
   mémoriser) de 1.96

En `R`
--------

.. code:: 

    %R qnorm(0.025)




.. parsed-literal::

    array([-1.95996398])



.. code:: 

    %R qnorm(0.975)




.. parsed-literal::

    array([ 1.95996398])



En `scipy.stats`
------------------

.. code:: 

    N = st.norm()
    N.isf(0.975)




.. parsed-literal::

    -1.959963984540054



.. code:: 

    N.isf(0.025)




.. parsed-literal::

    1.9599639845400545



.. code:: 

    x = linspace(-3.5,3.5,100)
    p = plot(x,N.pdf(x))
    vlines(-1.96,0,0.45)
    vlines(1.96,0,0.45)
    u = np.where((x>-1.96) & (x < -1.96))
    fill_between(x[u],N.pdf(x[u]),alpha=0.5)




.. parsed-literal::





.. image:: IntervallesDeConfianceE_files/IntervallesDeConfianceE_7_1.png


.. math:: \mathbb{P}(Z<-1.96)=0.025

.. math:: \mathbb{P}(Z< 1.96)=0.975

:math:`2.5\%` de chaque côté de la cloche qui font au total :math:`5\%`
à l'extérieur d'un intervalle central (sous la cloche) qui contient
:math:`95\%` des valeurs .

Pour obtenir notre intervalle de confiance sur :math:`\mu` on réorganise
les inégalités

.. math::

   \mathbb{P}(u_{\alpha/2} \le \sqrt{N}
   \frac{\bar{X}-\mu}{\sigma_X} \le u_{1-\alpha/2})= 1-\alpha

.. math:: \mathbb{P}(\frac{u_{\alpha/2}\sigma_X}{\sqrt{N}} \le  \bar{X}-\mu \le \frac{u_{1-\alpha/2}\sigma_X}{\sqrt{N}})= 1-\alpha

.. math:: \mathbb{P}(\frac{-u_{\alpha/2}\sigma_X}{\sqrt{N}} \ge  \mu-\bar{X} \ge \frac{-u_{1-\alpha/2}\sigma_X}{\sqrt{N}})= 1-\alpha

.. math:: \mathbb{P}(\frac{-u_{\alpha/2}\sigma_X}{\sqrt{N}}+\bar{X} \ge  \mu \ge \frac{-u_{1-\alpha/2}\sigma_X}{\sqrt{N}}+\bar{X})= 1-\alpha

On a vu que

.. math:: u_{0.025}=-1.96

et que

.. math:: u_{1-0.025}=1.96

.. math:: \mathbb{P}(1.96\frac{\sigma_X}{\sqrt{N}}+\bar{X} \ge  \mu \ge -1.96 \frac{\sigma_X}{\sqrt{N}}+\bar{X})= 1-\alpha

L'intervalle de confiance à :math:`95\%` est :

.. math:: [\bar{X}-1.96 \frac{\sigma_X}{\sqrt{N}}+,\bar{X}+1.96 \frac{\sigma_X}{\sqrt{N}}]

L'intervalle de confiance à :math:`90\%` est :

.. math:: [\bar{X}-1.64 \frac{\sigma_X}{\sqrt{N}}+,\bar{X}+1.64 \frac{\sigma_X}{\sqrt{N}}]

Observez que l'intervalle se réduit à proportion inverse de la racine
carré du nombre d'échantillons.

Intervalle de confiance pour l'estimation de la variance d'un échantillon normal
--------------------------------------------------------------------------------

Soit un échantillon :math:`X_N=\{x_1,...,x_N\}` indépendant, gaussien
:math:`\sim \mathcal{N}(\mu,\sigma)`

Il existe deux estimateurs empiriques (basé sur l'expérience, ici les
données) de la variance

.. math:: s^2=\frac{1}{N}\sum_{i=1}^n (x_i-\bar{x})^2

On peut montrer (exercice) que cet estimateur est biaisé et convergent

.. math:: \mathbb{E}(s^2)=\frac{n-1}{n}\sigma^2

et

.. math:: var(s^2) =\frac{2(n-1)}{n^2}\sigma^4

L'estimateur non biaisé de la variance est construit ainsi

.. math:: \tilde{s}^2=\frac{n}{n-1}s^2

De tel sorte que par construction

.. math:: \mathbb{E}(\tilde{s}^2)=\sigma^2

.. math:: var(\tilde{s}^2) =(\frac{n}{n-1})^2 \frac{2(n-1)}{n^2}\sigma^4= \frac{2\sigma^4}{n-1}

Cette différence entre les deux estimateurs ne se fait vraiment sentir
que pour les échantillons de très petite taille

.. math:: s^2=\frac{1}{N}\sum_{i=1}^n (x_i-\bar{x})^2=\frac{1}{N}\sum_{i=1}^n x_i^2+\bar{x}^2-\frac{2\bar{x}}{N}\sum_{i=1}^n x_i

.. math:: s^2=\frac{1}{N}\sum_{i=1}^N x_i^2+\bar{x}^2-2\bar{x}^2

.. math:: s^2=\frac{1}{N}\left(\sum_{i=1}^N x_i^2\right)-\bar{x}^2

On retrouve (moralement) la version empirique de

.. math:: var(X) = E(X^2)-E(X)^2

On peut **(dé)**\ montrer que

.. math:: \frac{Ns^2}{\sigma^2} \sim \chi^2_{(N-1)}

Nous allons ici seulement vérifier la validité expérimentale de cette
relation à l'aide d'une expérience aléatoire

.. code:: 

    sigma = 2.3  #cette valeur n'est pas supposée connue
    mu = 1.7 #cette valeur n'est pas supposée connue
    N = 10
    LN=st.norm(loc=mu,scale=sigma)
    X=LN.rvs(N)
    hlines(mu,0,N,'r')
    plot(X,'ob')




.. parsed-literal::





.. image:: IntervallesDeConfianceE_files/IntervallesDeConfianceE_9_1.png


.. code:: 

    Xbar = mean(X)
    print "moyenne empirique:",Xbar


.. parsed-literal::

    moyenne empirique: 1.86854987506


.. code:: 

    s2 = sum((X-Xbar)*(X-Xbar))/N
    print "variance empirique",s2
    print "écart type empirique",sqrt(s2)


.. parsed-literal::

    variance empirique 5.81850095919
    écart type empirique 2.41215691015


On va répéter le calcul de la variance empirique un grand nombre de
fois, et comparer la distribution de $:raw-latex:`\frac{Ns^2}{\sigma^2}`
$obtenue à la loi du :math:`\chi^2_{N-1}`

.. code:: 

    Ntrial = 10000
    tZ = []
    for i in range(Ntrial):
        X=LN.rvs(N)
        Xbar = mean(X)
        s2 = sum((X-Xbar)*(X-Xbar))/N
        Z = N*s2/sigma**2
        
        tZ.append(Z)

.. code:: 

    h = hist(tZ,100,normed=True)
    CN = st.chi2(N)
    CNm1=st.chi2(N-1)
    pN=CN.pdf(h[1])
    pNm1=CNm1.pdf(h[1])
    plot(h[1],pNm1,'g',lw=3,label=r"$\chi^2_{N-1}$")
    plot(h[1],pN,'r',label=r"$\chi^2_N$")
    legend()




.. parsed-literal::





.. image:: IntervallesDeConfianceE_files/IntervallesDeConfianceE_14_1.png


On vérifie bien l'adéquation

.. math:: \frac{Ns^2}{\sigma^2} \sim \chi^2_{(N-1)}

On peut montrer que :math:`\bar{x}` et :math:`s^2` sont des variables
aléatoires indépendantes

On forme la statistique suivante à partir de la moyenne et de la
variance empirique.

.. math:: T = \sqrt{N-1} \frac{ \bar{X}-\mu}{s}

Il est possible d'écrire

.. math:: T = \sqrt{N-1} \frac{ \frac{\bar{X} - \mu} {\sigma / \sqrt{N}}}{  \frac{s}{\sigma / \sqrt{n}}} 

.. math:: T = \frac{ \frac{\bar{X} - \mu} {\sigma / \sqrt{N}}}{  \sqrt{\frac{Ns^2} {(N-1)\sigma^2}}}

On reconnait donc au numérateur une variable pivotable
:math:`Z\sim\mathcal{N}(0,1)`

Si l'on pose à présent

.. math:: U=\frac{Ns^2}{\sigma^2}

On a montré (et non pas démontré) que

.. math::  U \sim \chi^2_{N-1}

On a donc

.. math:: T = \frac{Z}{\sqrt{\frac{U}{N-1}}}

Qui suit de par la définition d'une loi de Student

.. math:: T \sim \mathcal{S}tudent(N-1) \square 

Si on désigne par :math:`t_{\alpha/2}^{N-1}` le quantile de la loi
:math:`\mathcal{S}tudent(N-1)` au niveau :math:`\alpha/2`

L'intervalle de confiance pour la moyenne lorsque la variance **vraie**
est inconnue et qu'elle est donc remplacée par une estimation à partir
des échantillons est donné par :

.. math:: \mathbb{P}\left (\mu \in \left[ \bar{X} + \frac{t_{\alpha/2}^{(N-1)}}{\sqrt{N-1}} s , \bar{X} + \frac{t_{1-\alpha/2}^{(N-1)}}{\sqrt{N-1}}s \right] \right)=1-\alpha

.. math:: \mathbb{P}\left (\mu \in \left[ \bar{X} + \frac{t_{\alpha/2}^{(N-1)}}{\sqrt{N}} \tilde{s} , \bar{X} + \frac{t_{1-\alpha/2}^{(N-1)}}{\sqrt{N}} \tilde{s} \right] \right)=1-\alpha

Vérification avec le module `t.test` de `R`
-----------------------------------------------

.. code:: 

    D = np.random.randint(0,100,15)
    print D


.. parsed-literal::

    [14 43 49 54 56 75 27 52 95 37 19 11 61 78 29]


.. code:: 

    %Rpush D

.. code:: r

    %%R 
    t.test(D)



.. parsed-literal::

    
    	One Sample t-test
    
    data:  D
    t = 7.3737, df = 14, p-value = 3.491e-06
    alternative hypothesis: true mean is not equal to 0
    95 percent confidence interval:
     33.09270 60.24064
    sample estimates:
    mean of x 
     46.66667 
    



.. code:: 

    N = len(D)
    mu = mean(D)
    s  = sqrt(N*(mean(D*D)-mu**2)/(N-1.))
    s1 = std(D)
    t = st.t(N)
    print "écart biaisé",s1
    print "écart type non biaisé",s
    u=t.isf(0.025)


.. parsed-literal::

    écart biaisé 23.6802777761
    écart type non biaisé 24.5114161935


.. code:: 

    print mean(D)-u*s/sqrt(N)
    print mean(D)+u*s/sqrt(N)


.. parsed-literal::

    33.1771052004
    60.156228133


.. code:: 

    print mean(D)-u*s1/sqrt(N-1)
    print mean(D)+u*s1/sqrt(N-1)


.. parsed-literal::

    33.1771052004
    60.156228133


En résumé
---------

En pratique on part d'un échantillon de données dont on ne connait pas
forcément ni la loi ni les paramètres de la loi.

On se pose la question de **la confiance** que l'on doit avoir sur le
calcul de la moyenne empirique vis à vis de la "vraie" valeur de la
moyenne de la loi sous-jacente.

L'intervalle de confiance dépend du nombre d'échantillons :math:`N`

L'intervalle de confiance est une quantité aléatoire car il dépend de
l'échantillon de données.

L'intervalle de confiance est centré sur la moyenne empirique.

L'intervalle de confiance peut ne pas conenir la vrai valeur. C'est la
proportion de ces situations qui est controlé par la valeur de la
confiance demandée 95% , 99.999999% ,....

.. code:: r

    %%R
    hommes = c(172.5, 175, 176, 177, 177, 178.5, 179, 179, 179.5, 180)
    femmes = c(167, 168, 168.5, 170, 171, 175, 175, 176)
    t.test(hommes)$conf.int
    t.test(femmes)$conf.int



.. parsed-literal::

    [1] 168.3404 174.2846
    attr(,"conf.level")
    [1] 0.95


