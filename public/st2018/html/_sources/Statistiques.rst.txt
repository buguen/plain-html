.. raw:: html

   <!-- + [Ref](./BayesianTechniques/lecture05.pdf)
       
   + [Jeffreys Prior](/home/uguen/Documents/ens/insa/M4S2/texte/pdf/Subjective_probability_Jeffreys.pdf)

   http://nbviewer.ipython.org/url/jakevdp.github.io/downloads/notebooks/FreqBayes.ipynb

   -->

Inférence de la variance dans un modèle Gaussien
================================================

On considère une variable aléatoire :math:`y_i` distribuée selon une loi
normale de moyenne connue et de variance :math:`\phi` inconnue.

.. math:: y_i \sim \mathcal{N}(m,\phi)

.. math:: p(y_i) = \frac{1}{\sqrt{2 \pi \phi}} e^{-\frac{(y_i-m)^2}{2\phi}}

On considère un vecteur de :math:`n` variable :math:`y_i` indépendantes.
Ce vecteur possède donc une densité de probabilité qui s'exprime (du
fait de la propriété d'indépendance) comme le produit des densités
marginales de chacun des éléments du vecteur.

.. math:: p(y_1,y2,...,y_n|\phi) \propto \Pi_{i=1}^{n} \frac{1}{\sqrt{2 \pi \phi}} e^{-\frac{(y_i-m)^2}{2\phi}}

Seule la proportionalité à une constante près nous intéresse ici, car
puisqu'il s'agit d'une densité de probabilité il est toujours possible
de renormaliser à posteriori.

.. math:: p(y_1,y2,...,y_n|\phi) \propto \frac{1}{\phi^{n/2}} e^{-\frac{\sum_{i=1}^{n}(y_i-m)^2}{2\phi}}

Si l'on appelle `précision` la quantité inversement proportionelle à
la variance

.. math:: \tau=\frac{1}{\phi}

 et que l'on pose

.. math:: n s_n^2 = \sum_{i=1}^{n} (y_i-m)^2

Il vient

.. math:: p(y_1,y2,...,y_n|\tau) = p(\mathbf{y}|\tau) \propto \tau^{n/2} e^{-\frac{n s_n^2}{2}\tau }

Forme de la loi a priori sur la `précision`
---------------------------------------------

| La précision est une quantité positive inconnue.
| Si l'on suppose que la la précsion :math:`\tau` suit une loi a priori
  en :math:`\Gamma(\alpha,\beta)`. Cette loi englobe une grande variété
  de situations. Pour :math:`\alpha` grand on tend vers une loi normale.

De sorte que

:math:`p_{\tau}(\alpha,\beta) \propto \tau^{\alpha-1} e^{-\beta \tau}`

de façon précise en tenant compte du facteur de normalisation

.. math:: p_{\tau}(\alpha,\beta) = \frac{\beta^{\alpha}\tau^{\alpha-1} e^{-\beta \tau}}{\Gamma(\alpha)}

`Loi Gamma <http://fr.wikipedia.org/wiki/Loi_Gamma>`__

Muni de cette loi a priori, on peut calculer la loi a posteriori, en
utilisant la formule de Bayes.

.. math:: p(\tau|\mathbf{y}) \propto p(\mathbf{y}|\tau) p(\tau)

Cette loi a priori est formellement identique à la loi a posteriori.

.. math:: p(\tau|\mathbf{y}) \propto \tau^{n/2} e^{-\frac{n s_n^2}{2}\tau } \tau^{\alpha-1} e^{-\beta\tau}

.. math:: p(\tau|\mathbf{y}) \propto \tau^{n/2+\alpha-1} e^{-(\frac{n s_n^2}{2}+\beta)\tau }

Cette loi a posteriori est encore une loi
:math:`\Gamma(\alpha+n/2,\beta+\frac{ns_n^2}{2})`. On constate que la
variance va être de plus en plus concentrée autour de sa valeur la plus
vraisemblable à mesure que le nombre d'observations :math:`n` augmente.

Par ailleurs si :math:`X` est une v.a qui suit la loi
:math:`\Gamma(\alpha`,\ :math:`\beta)`, son espérance et son mode son
respectivement

.. math:: \mathbb{E}_{X}=\frac{\alpha}{\beta}

.

.. math:: \textrm{mode}_X=\frac{\alpha-1}{\beta} \; \textrm{pour}\; \alpha>1

.. math:: \mathbb{E}(\tau|\mathbf{y}) = \frac{\alpha+\frac{n}{2}}{\beta+\frac{ns_n^2}{2}} 

.. math:: \textrm{mode}(\tau|\mathbf{y}) = \frac{\alpha - 1+\frac{n}{2}}{\beta+\frac{ns_n^2}{2}} 

.. math::

   \lim_{n\rightarrow +\infty}\mathbb{E}(\tau|\mathbf{y}) = \lim_{n\rightarrow
   +\infty} \frac{\alpha+\frac{n}{2}}{\beta+\frac{ns_n^2}{2}} = \frac{1}{s_n^2} 

Le mode a évidemment même limite que l'espérance conditionelle.
Rappelons que dans le cas Gaussien mode, moyenne et médiane sont
confondues.

A partir de la densité a postériori de la précision, on peut déduire
c'elle de la variance.

Méthode du Jacobien
~~~~~~~~~~~~~~~~~~~

.. math:: p_{\phi}(\phi) = p_{\tau}(\frac{1}{\phi})| \frac{d\tau}{d\phi}| 

.. math:: \frac{d\tau}{d\phi}=-\frac{1}{\phi^2}

.. math:: p_{\phi}(\phi) = p_{\tau}(\frac{1}{\phi}) \frac{1}{\phi^2}

.. math:: p_{\phi}(\phi) = \frac{\beta^{\alpha}(1/\phi)^{\alpha-1} e^{-\beta/\phi}}{\Gamma(\alpha)}  \frac{1}{\phi^2}

.. math:: p_{\phi}(\phi) = \frac{\beta^{\alpha}(1/\phi)^{\alpha+1} e^{-\beta/\phi}}{\Gamma(\alpha)}  

Cette densité est une loi gamma inverse

`Loi inverse-gamma <http://fr.wikipedia.org/wiki/Loi_inverse-gamma>`__

Son espérance et son mode sont donnés respectivement par :

.. math:: \mathbb{E}_{\phi}=\frac{\beta}{\alpha-1}

.

.. math:: \textrm{mode}_{\phi}=\frac{\beta}{\alpha+1}\; \textrm{pour}\; \alpha>1

Démonstration : calcul de l'espérance
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. math:: \mathbb{E}_{\phi} = \int_{0}^{+\infty} \phi \frac{\beta^{\alpha}(1/\phi)^{\alpha+1} e^{-\beta/\phi}}{\Gamma(\alpha)}  d\phi

.. math:: \mathbb{E}_{\phi} =  \frac{\beta^{\alpha}}{\Gamma(\alpha)} \int_{0}^{+\infty} \phi (1/\phi)^{\alpha+1} e^{-\beta/\phi} d\phi

.. math:: \mathbb{E}_{\phi} =  \frac{\beta^{\alpha}}{\Gamma(\alpha)} \int_{0}^{+\infty} \phi^{-\alpha} e^{-\beta/\phi} d\phi

.. math:: u = \frac{\beta}{\phi}

.. math:: \phi = \frac{\beta}{u}

.. math:: d\phi = \frac{-\beta du}{u^2}

.. math:: \mathbb{E}_{\phi} =  \frac{\beta^{\alpha}}{\Gamma(\alpha)} \int_{0}^{+\infty} (\frac{\beta}{u})^{-\alpha} e^{-u} \frac{\beta du}{u^2}

.. math:: \mathbb{E}_{\phi} =  \frac{\beta}{\Gamma(\alpha)} \int_{0}^{+\infty} (\frac{1}{u})^{-\alpha} e^{-u} \frac{du}{u^2}

.. math:: \mathbb{E}_{\phi} =  \frac{\beta}{\Gamma(\alpha)} \int_{0}^{+\infty} (u)^{\alpha-2} e^{-u} du

.. math:: \mathbb{E}_{\phi} =  \frac{\beta}{\Gamma(\alpha)} \Gamma(\alpha-1)

.. math:: \mathbb{E}_{\phi} =  \frac{\beta}{(\alpha-1)\Gamma(\alpha-1)} \Gamma(\alpha-1)

.. math:: \mathbb{E}_{\phi} =  \frac{\beta}{(\alpha-1)}

Démonstration calcul du mode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. math:: p_{\phi}(\phi) = \frac{\beta^{\alpha}(1/\phi)^{\alpha+1} e^{-\beta/\phi}}{\Gamma(\alpha)}  

On forme et on annule la dérivé de la densité

.. math:: \frac{dp_{\phi}(\phi)}{d\phi} = 0 

.. math::  \frac{d \phi^{-(\alpha+1)} e^{-\beta/\phi}}{d\phi} = 0

.. math::  -(\alpha+1)\phi^{-(\alpha+1)-1} e^{-\beta/\phi} + \phi^{-(\alpha+1)}(\beta/\phi^2)e^{-\beta/\phi} = 0

.. math::  -(\alpha+1)\phi^{-\alpha-2} + \phi^{-\alpha-3}\beta = 0

.. math::  -(\alpha+1)+ \phi^{-1}\beta = 0

.. math::   \phi^{-1}\beta = \alpha+1

.. math::   \phi = \frac{\beta}{\alpha+1}

La variance de la loi Gamma inverse est pour :math:`\alpha>2`

.. math:: \mathbb{V}_X = \frac{\beta^2}{(\alpha-1)^2(\alpha-2)}

Puisque la loi a posteriori de la variance est une loi inverse gamma

.. math:: \textrm{InvGamma}(\alpha+n/2,\beta+\frac{ns_n^2}{2})

.. math:: \mathbb{V}_{\phi}=\frac{(\beta+\frac{ns_n^2}{2})^2}{(\alpha+n/2-1)^2(\alpha+n/2-2)^2}

On peut donc calculer une approximation de l'intervalle de confiance de
l'estimateur de la variance comme

.. math:: \hat{\phi}=\frac{\beta+(n s_n^2)/2}{\alpha+n/2-1} \pm 1.96 \sqrt{\mathbb{V}_{\phi}(\phi | \mathbf{y})}

Ou encore en utilisant la fonction inverse de la cdf de la distribution
inverse Gamma.

.. code:: 

    ig1 = st.invgamma(1)
    ig2 = st.invgamma(2)
    ig5 = st.invgamma(5)

.. code:: 

    x = np.arange(0.01,3,0.01)
    pig1 = ig1.pdf(x)
    pig2 = ig2.pdf(x)
    pig5 = ig5.pdf(x)

.. code:: 

    plt.plot(x,pig5,label=u'$\\alpha$ =5')
    plt.plot(x,pig2,label=u'$\\alpha$ =2')
    plt.plot(x,pig1,label=u'$\\alpha$ =1')
    plt.legend()




.. parsed-literal::





.. image:: StatistiquesE_files/StatistiquesE_4_1.png


Expérience numérique
~~~~~~~~~~~~~~~~~~~~

:math:`\alpha` est un facteur de forme et :math:`\beta` est un facteur
d'échelle.

Modèle a priori hyper paramètres.

.. code:: 

    alpha = 2
    beta  = 2 
    Ntrial = 1000

.. code:: 

    Ltau = st.gamma(alpha,scale=beta)

.. code:: 

    x = np.arange(0.01,16,0.01)

.. code:: 

    px = Ltau.pdf(x)

.. code:: 

    plt.plot(x,px)




.. parsed-literal::





.. image:: StatistiquesE_files/StatistiquesE_10_1.png


On tire :math:`N_{trial}` valeur de variance selon la loi connue a
priori.

.. code:: 

    ltau = Ltau.rvs(Ntrial)
    lphi = 1/ltau
    plt.plot(lphi)




.. parsed-literal::





.. image:: StatistiquesE_files/StatistiquesE_12_1.png


On procède au tirage de :math:`n` échantillons

.. code:: 

    n = 3

.. code:: 

    lt = np.arange(Ntrial)
    le1 = []
    le2 = []
    le3 = []
    alphau = 1
    betau = 2
    for phi in lphi:
        LN = st.norm(0,scale=np.sqrt(phi))
        y = LN.rvs(n)
        sn2 = sum(y*y)/n
        phi_e1 = (betau+n*sn2/2.)/(alphau+n/2.+1)
        phi_e2 = sn2*n/(n-1) 
        le1.append(phi_e1)
        le2.append(phi_e2)
        le3.append(sn2)

.. code:: 

    plt.figure(figsize=(10,10))
    plt.semilogy(lt,lphi,'.b',label='True Variance')
    plt.semilogy(lt,le1,'.r',label='Estimated Variance Bayesian')
    plt.semilogy(lt,le2,'.g',label='Estimated Variance (n-1)')
    plt.semilogy(lt,le3,'.c',label='Estimated Variance (n)')
    plt.legend()




.. parsed-literal::





.. image:: StatistiquesE_files/StatistiquesE_16_1.png


.. raw:: html

   <!-- http://nbviewer.ipython.org/urls/gist.github.com/mattions/6113437/raw/c5468ea930d6960225d83e112d7f3d00d9c13398/Exploring+different+distribution.ipynb
   -->

.. code:: 

    e1 = le1-lphi
    e2 = le2-lphi
    e3 = le3-lphi
    E1 = sum(e1*e1)/len(e1)
    E2 = sum(e2*e2)/len(e2)
    E3 =sum(e3*e3)/len(e3)
    print E1
    print E2
    print E3


.. parsed-literal::

    0.626241263439
    18.5599335624
    6.54407098148


.. code:: 

    plt.plot(lt,e1,'.b',label='Bayesian')
    plt.plot(lt,e2,'.g',label='unbiased')
    plt.plot(lt,e3,'.r',label='biased')
    plt.legend()




.. parsed-literal::





.. image:: StatistiquesE_files/StatistiquesE_19_1.png


.. code:: 

    plt.figure(figsize=(10,10))
    plt.hist(e1,100,alpha=0.3,color='r')
    plt.hist(e2,100,alpha=0.3,color='b')
    plt.hist(e3,100,alpha=0.3,color='g')




.. parsed-literal::

    (array([   2.,    0.,    0.,    0.,    2.,    0.,    8.,   73.,  825.,
              71.,   12.,    4.,    0.,    1.,    1.,    0.,    0.,    0.,
               0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,
               0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,
               0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,
               0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,
               0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,
               0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,
               0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,
               0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,
               0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,    1.]),
     array([ -7.33215267,  -6.47139898,  -5.61064528,  -4.74989159,
             -3.8891379 ,  -3.02838421,  -2.16763052,  -1.30687682,
             -0.44612313,   0.41463056,   1.27538425,   2.13613795,
              2.99689164,   3.85764533,   4.71839902,   5.57915272,
              6.43990641,   7.3006601 ,   8.16141379,   9.02216749,
              9.88292118,  10.74367487,  11.60442856,  12.46518226,
             13.32593595,  14.18668964,  15.04744333,  15.90819703,
             16.76895072,  17.62970441,  18.4904581 ,  19.3512118 ,
             20.21196549,  21.07271918,  21.93347287,  22.79422657,
             23.65498026,  24.51573395,  25.37648764,  26.23724133,
             27.09799503,  27.95874872,  28.81950241,  29.6802561 ,
             30.5410098 ,  31.40176349,  32.26251718,  33.12327087,
             33.98402457,  34.84477826,  35.70553195,  36.56628564,
             37.42703934,  38.28779303,  39.14854672,  40.00930041,
             40.87005411,  41.7308078 ,  42.59156149,  43.45231518,
             44.31306888,  45.17382257,  46.03457626,  46.89532995,
             47.75608365,  48.61683734,  49.47759103,  50.33834472,
             51.19909842,  52.05985211,  52.9206058 ,  53.78135949,
             54.64211318,  55.50286688,  56.36362057,  57.22437426,
             58.08512795,  58.94588165,  59.80663534,  60.66738903,
             61.52814272,  62.38889642,  63.24965011,  64.1104038 ,
             64.97115749,  65.83191119,  66.69266488,  67.55341857,
             68.41417226,  69.27492596,  70.13567965,  70.99643334,
             71.85718703,  72.71794073,  73.57869442,  74.43944811,
             75.3002018 ,  76.1609555 ,  77.02170919,  77.88246288,  78.74321657]),
     <a list of 100 Patch objects>)




.. image:: StatistiquesE_files/StatistiquesE_20_1.png

