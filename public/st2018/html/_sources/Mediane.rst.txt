.. sectnum:: 
    :prefix: 2. 
    :start: 7

.. _Mediane:

Médiane
=======

La médiane est la valeur qui sépare une population ou un échantillon en
deux parties égales

Médiane d'une loi continue
--------------------------

Soit X une variable aléatoire continue définie sur :math:`\mathbb{R}`

La médiane est la borne de l'intégrale telle que :

.. math::


   \int_{-\infty}^{\textrm{med}(X)}p_X(x)dx = \int_{\textrm{med}(X)}^{+\infty}p_X(x)dx =\frac{1}{2} 

La médiane sépare l'espace de la v.a en 2 sous ensembles equiprobables.

Médiane d'un échantillon de taille :math:`N`
--------------------------------------------

Pour déterminer la médiane d'un ensemble de valeurs, il suffit
d'ordonner les valeurs en une liste croissante et de choisir la valeur
qui est au centre de cette liste. Dans le cas :

-  d'un nombre impair de points, la médiane appartient au jeu de données
-  d'un nombre pair de points, la médiane n'appartient pas au jeu de
   données.

.. code:: 

    X = randint(0,100,10)
    print(X)
    Xs = sort(X)
    print("Mediane : ",median(X))
    print(Xs[0:len(X)/2])
    print(Xs[len(X)/2:])
    print('('+str(Xs[len(X)/2-1])+' + '+str(Xs[len(X)/2])+')/2',(Xs[len(X)/2-1]+Xs[len(X)/2])/2.)


.. parsed-literal::

    [ 2 79 63 20 71 34 34 25 64 64]
    ('Mediane : ', 48.5)
    [ 2 20 25 34 34]
    [63 64 64 71 79]
    ('(34 + 63)/2', 48.5)

