Simulation d'une variable aléatoire Gaussienne
==============================================

Nous allons simuler une variable aléatoire :math:`X` Gaussienne de
moyenne nulle et de variance unité.

:math:`X\sim \mathcal{N}(0,1)`

Nous générons un échantillon :math:`X_N=\{x_1,....,x_N\}` de taille
:math:`N=100`.

-  Il convient de bien distinguer la variable aléatoire :math:`X` de
   l'échantillon :math:`X_N` issu d'une *réalisation* de la loi
   :math:`X` et de taille finie N.
-  La statistique procède du traitement de :math:`X_N` est s'appuyant
   sur la modélisation probabiliste de **variables aléatoires
   sous-jacentes** qui sont un modèle mathématique associé à
   l'observation finie :math:`X_N`.
-  La question de "la meilleure loi" à choisir pour "coller" à une
   observation donnée est un problème de statistique. (test
   d'adéquation)

Génération du jeu de donnéés et calcul de paramètres statistiques de
localisation (moyenne) et de dispersion (écart type)

.. code:: 

    N = 100
    d = randn(100)
    m = d.mean()
    s = d.std()
    
    print "La moyenne empirique des "+str(N)+" echantillons est : ",str(m)
    print "L'cart type empirique des "+str(N)+" echantillons est : ",str(s)


.. parsed-literal::

    La moyenne empirique des 100 echantillons est :  -0.0349396256112
    L'cart type empirique des 100 echantillons est :  0.968843362127


Histogramme
-----------

Affichons l'histogramme des données avec une segmentation de
l'intervalle :math:`[x_{min}, x_{max}]` en 15 sous intervalles égaux.

Dans chaque sous-intervalle, on compte le nombre de valeurs observées,
et on reporte cet entier sous la forme d'un rectangle de hauteur
proportionnelle à cette valeur.

.. code:: 

    hist(d, 15)
    m_y = 2
    plot(m, m_y, "ko")
    plot([m - s, m + s], [m_y] * 2, "k--");



.. image:: SimulationGaussienneE_files/SimulationGaussienneE_4_0.png


Affichons à présent la médiane et les quartiles.

.. code:: 

    med = median(d)
    iqr = percentile(d, [25, 75])
    print "mediane : ",med
    print "interquartile (25%,75%) : ", iqr
    hist(d, 15)
    plot(med, m_y, "ko")
    plot(iqr, [m_y] * 2, "k--");


.. parsed-literal::

    mediane :  -0.0304916497961
    interquartile (25%,75%) :  [-0.62079078  0.51555955]



.. image:: SimulationGaussienneE_files/SimulationGaussienneE_6_1.png


On peut observer que pour cette loi symétrique, la moyenne et la médiane
sont trés proches.

C'est l'une des propriétés des **lois symétriques**, comme c'est ici le
cas pour la **loi normale.**

L'intervalle :

.. math:: [\mu -\sigma_x, \mu+\sigma_x] 

s'étend du 16ième au 84ième percentile.

-  16 % sous la valeur minimale de l'intervalle
-  68 % dans l'intervalle
-  16 % au dessus de la valeur maximale de l'intervalle

Dans la séquence d'instructions suivante on fait appel à l'importante
fonction `sf` (survival function) qui permet de connaître la
probabilité d'être **au dessus** d'un seuil. C'est la fonction
complémentaire de la fonction de répartition (cummulative distribution
function).

On a les relations suivantes :

.. math:: sf(x) = Proba[X>x]

.. math:: cdf(x) = Proba[X<x]

.. math:: cdf(x) + sf(x) = 1

.. math:: sf(x) = 1 -cdf(x)

.. code:: 

    # vérification
    N = st.norm()
    p16=1-N.sf(-1)
    print("P16 : ", p16)
    p84=N.sf(1)
    print("P84 : ", p84)
    print 1-(p16+p84)


.. parsed-literal::

    ('P16 : ', 0.15865525393145707)
    ('P84 : ', 0.15865525393145707)
    0.682689492137

