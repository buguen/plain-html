rvs(loc=0, scale=1, size=1, random_state=None) Random variates. pdf(x,
loc=0, scale=1) Probability density function. logpdf(x, loc=0, scale=1)
Log of the probability density function. cdf(x, loc=0, scale=1)
Cumulative density function. logcdf(x, loc=0, scale=1) Log of the
cumulative density function. sf(x, loc=0, scale=1) Survival function (1
- cdf — sometimes more accurate). logsf(x, loc=0, scale=1) Log of the
survival function. ppf(q, loc=0, scale=1) Percent point function
(inverse of cdf — percentiles). isf(q, loc=0, scale=1) Inverse survival
function (inverse of sf). moment(n, loc=0, scale=1) Non-central moment
of order n stats(loc=0, scale=1, moments='mv') Mean(‘m’), variance(‘v’),
skew(‘s’), and/or kurtosis(‘k’). entropy(loc=0, scale=1) (Differential)
entropy of the RV. fit(data, loc=0, scale=1) Parameter estimates for
generic data. expect(func, loc=0, scale=1, lb=None, ub=None,
conditional=False, \*\*kwds) Expected value of a function (of one
argument) with respect to the distribution. median(loc=0, scale=1)
Median of the distribution. mean(loc=0, scale=1) Mean of the
distribution. var(loc=0, scale=1) Variance of the distribution.
std(loc=0, scale=1) Standard deviation of the distribution.
interval(alpha, loc=0, scale=1) Endpoints of the range that contains
alpha percent of the distribution
